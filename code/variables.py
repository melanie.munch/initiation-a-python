valeur1 = 5 # Int
valeur2 = 3.8 # Float
texte = "cou" # String
booleen = True # Bool

# Addition
nouvelleValeur = valeur1 + valeur 1
print(nouvelleValeur, type(nouvelleValeur) ) # 10 int

nouvelleValeur = valeur1 + valeur2
print(nouvelleValeur, type(nouvelleValeur) ) # 8.8 float

nouveauTexte = texte + texte
print(nouveauTexte, type(nouveauTexte) ) # coucou str

# Multiplication
nouvelleValeur = valeur1 * 3
print(nouvelleValeur, type(nouvelleValeur) ) # 15 int

nouveauTexte = texte * 3
print(nouveauTexte, type(nouveauTexte) ) # coucoucou str

# Tests
aimeLesChats = True
aimeLesChiens = True

if aimeLesChats and aimeLesChiens:
	print("Vous aimez les animaux")
elif aimeLesChats and not aimeLesChiens:
	print("Vous preferez les chats")
elif not aimeLesChats and aimeLesChiens:
	print("Vous preferez les chiens")
else:
	print("Vous n'aimez ni les chats ni les chiens")