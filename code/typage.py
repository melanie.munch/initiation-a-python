# Important! La fonction input renvoie toujours un string
monAge = input("Quel age avez-vous?")
print(type(monAge)) # str 
print(monAge + 5) # Renvoie une erreur, car on ne peut pas additionner un str et un int

# Str -> int
monAge = int(monAge) # On force la transformation de monAge en int
print(monAge + 5) # Renvoie bien une valeur de type int

# Float -> int
pi = 3.14
pi_arrondi = int(pi)
print(pi_arrondi) # 3