def maSomme(valeur1, valeur2):
    """
    Fonction renvoyant le double de la somme entre valeur1 et valeur2.
    Indique si le resultat depasse 100.
    valeur1: int
    valeur2: int
    -> resultat: int
    """
	resultat = (valeur1+valeur2)*2
	if resultat > 100:
		print("Le double de la somme de {} et {} est strictement plus grand que 100".format(valeur1, valeur2)
	return(resultat)
	
var1 = 5
var2 = 10

resultat1 = maSomme(var1, var2)
resultat2 = maSomme(var1, var1)
resultat3 = maSomme(resultat1, resultat2)