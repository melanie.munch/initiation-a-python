# -*- coding: utf-8 -*-
"""
@author: Melanie
"""
from math import pi # pi renvoie la valeur exacte de pi

####
# Fonction

def aire_carre(l):
    """Renvoie l'aire d'un carre de largeur l
    l: int ou float (float inclue int, on ne precisera que float par la suite)
    -> aire: float
    """
    aire = l*l
    return(aire)

def aire_rectangle(l, L):
    """Renvoie l'aire d'un rectangle de largeur l et longueur L
    l: float
    L: float
    -> aire: float
    """
    aire = l*L
    return(aire)

def aire_cercle(r):
    """Renvoie l'aire d'un cercle de rayon r.
    Note: la variable pi doit être appelée via l'import "from math import pi" si on veut l'utiliser.
    r: float
    -> aire: float
    """
    aire = pi*r**2
    return(aire)

def table(N):
    """Imprime la table de multiplication de N.
    N: float
    """
    for k in range(10):
        print("{} x {} = {}".format(k, N, k*N))

def divise(a,b):
    """Renvoie True si a divise b ou si b divise a, et non sinon.
    a: int
    b: int
    -> boolean
    """
    if a%b == 0:
        return(True)
    elif b%a == 0:
        return(True)
    else:
        return(False)
    
def premier(a):
    """Renvoie True si a est un nombre premier, i.e. s'il n'est divisible que par 1
    et lui meme.
    a: int
    -> boolean
    """
    premier = True # On suppose qu'il est premier, et on verifie si c'est toujours verifie
    
    for k in range(2,a):
        if divise(a, k):
            premier = False # On a rencontre un nombre qui divisait a autre que 1 et lui meme
    
    return(premier)

####
# Script

# Test des aires
largeur = 5
longueur = 8
rayon = 3
    
carre = aire_carre(largeur)
print("L'aire d'un carre de largeur {}cm est de {}cm².".format(largeur, carre))

rectangle = aire_rectangle(largeur, longueur)
print("L'aire d'un carre de largeur {}cm et de longueur {}cm est de {}cm².".format(largeur, longueur, rectangle))

cercle = aire_cercle(rayon)
print("L'aire d'un cercle de rayon {}cm est de {}cm².".format(rayon, cercle))

# Test des tables
table(7)

# Test des divisibles
a = 5
b = 2
if divise(a,b):
    print("{} et {} sont divisibles".format(a,b))
else:
    print("{} et {} ne sont pas divisibles".format(a,b))
    
# Test des nombres premiers
for k in range(11):
    if premier(k):
        print("{} est un nombre premier".format(k))
